<?php
include 'database.php';
include 'fungsi.php';
include 'csrf.php';

$id = anti($_POST['id']);
$nama_mahasiswa = convert("encrypt", anti($_POST['nama_mahasiswa']));
$jenkel = convert("encrypt", anti($_POST['jenkel']) );
$alamat = convert("encrypt", anti($_POST['alamat']) );
$jurusan = convert("encrypt", anti($_POST['jurusan']) );
$tanggal_masuk = anti($_POST['tanggal_masuk']);

if ($id == "") {
	$query = "INSERT into mahasiswa (nama_mahasiswa, alamat, jurusan, jenis_kelamin, tgl_masuk) VALUES (?, ?, ?, ?, ?)";
	$databes = $db1->prepare($query);
	$databes->bind_param("sssss", $nama_mahasiswa, $alamat, $jurusan, $jenkel, $tanggal_masuk);
	$databes->execute();
} else {
	$query = "UPDATE mahasiswa SET nama_mahasiswa=?, alamat=?, jurusan=?, jenis_kelamin=?, tgl_masuk=? WHERE id=?";
	$databes = $db1->prepare($query);
	$databes->bind_param("sssssi", $nama_mahasiswa, $alamat, $jurusan, $jenkel, $tanggal_masuk, $id);
	$databes->execute();
}
echo json_encode(['success' => 'Sukses']);
$db1->close();
?>