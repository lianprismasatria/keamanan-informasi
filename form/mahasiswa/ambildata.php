<?php
    include 'database.php';
    include 'fungsi.php';
    include 'csrf.php';

    $id = $_POST['id'];
    $query = "SELECT * FROM mahasiswa WHERE id=? ORDER BY id DESC";
    $databes = $db1->prepare($query);
    $databes->bind_param('i', $id);
    $databes->execute();
    $res1 = $databes->get_result();
    while ($row = $res1->fetch_assoc()) {
        $h['id'] = $row["id"];
        $h['nama_mahasiswa'] = convert("decrypt", $row["nama_mahasiswa"]);
        $h['alamat'] = convert("decrypt", $row["alamat"]);
        $h['jurusan'] = convert("decrypt", $row["jurusan"]);
        $h['jenis_kelamin'] = convert("decrypt", $row["jenis_kelamin"]);
        $h['tgl_masuk'] = $row["tgl_masuk"];
    }
    echo json_encode($h);
?>