<?php
	$notif = "";

	if (isset($_POST['submit'])) {
		$con = new mysqli('localhost', 'root', 'password', 'kaminfo');

		$name = $con->real_escape_string($_POST['nama']);
		$email = $con->real_escape_string($_POST['email']);
		$password = $con->real_escape_string($_POST['password']);
		$hPassword = $con->real_escape_string($_POST['hPassword']);

        $uppercase = preg_match('@[A-Z]@', $password);
        $lowercase = preg_match('@[a-z]@', $password);
        $number    = preg_match('@[0-9]@', $password);
        $specialChars = preg_match('@[^\w]@', $password);

		if ($password != $hPassword){
			$notif = "Password tidak cocok";
        }else if(!$uppercase || !$lowercase || !$number || !$specialChars || strlen($password) < 8) {
            echo "<p align='center', color='red'> Password harus minimal 8 karakter dan mempunyai 1 huruf besar, 1 huruf kecil dan karakter spesial </p>";
        }else {
			$hash = password_hash($password, PASSWORD_BCRYPT);
			$con->query("INSERT INTO user (nama,email,password) VALUES ('$name', '$email', '$hash')");
			$notif = "kamu sudah terdaftar";
		}
	}
?>



<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Daftar</title>

</head>


<link href="//netdna.bootstrapcdn.com/bootstrap/3.1.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<script src="//netdna.bootstrapcdn.com/bootstrap/3.1.0/js/bootstrap.min.js"></script>
<script src="//code.jquery.com/jquery-1.11.1.min.js"></script>

<body>

<div class="container">
        <div id="loginbox" style="margin-top:50px;" class="mainbox col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2">
            <div class="panel panel-info">
                <div class="panel-heading">
                    <div class="panel-title">Daftar</div>
                </div>

                <div style="padding-top:30px" class="panel-body">

                    <div style="display:none" id="login-alert" class="alert alert-danger col-sm-12"></div>
                    <?php if ($notif != "") echo $notif . "<br><br>"; ?>

                    <form method="post" action="daftar.php" class="form-horizontal" role="form">

                        <div style="margin-bottom: 25px" class="input-group">
                            <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
                            <input type="text" class="form-control" minlength="3" name="nama" placeholder="nama" required>
                        </div>

                        <div style="margin-bottom: 25px" class="input-group">
                            <span class="input-group-addon"><i class="glyphicon glyphicon-envelope"></i></span>
                            <input type="text" class="form-control" minlength="3" name="email" placeholder="email" required>
                        </div>

                        <div style="margin-bottom: 25px" class="input-group">
                            <span class="input-group-addon"><i class="glyphicon glyphicon-lock"></i></span>
                            <input type="password" minlength="8" class="form-control" name="password" placeholder="password" required>
                        </div>

                        <div style="margin-bottom: 25px" class="input-group">
                            <span class="input-group-addon"><i class="glyphicon glyphicon-lock"></i></span>
                            <input type="password" minlength="8" class="form-control" name="hPassword" placeholder="ketik ulang password" required>
                        </div>


                        <div style="margin-top:10px" class="form-group">
                            <!-- Button -->

                            <div class="col-sm-12 controls">
                            <input class="btn btn-success" name="submit" type="submit" value="DAFTAR">
                            </div>
                        </div>


                        <div class="form-group">
                            <div class="col-md-12 control">
                                <div style="border-top: 1px solid#888; padding-top:15px; font-size:85%">
                                    Sudah terdaftar?
                                    <a href="login.php">
                                        Masuk
                                    </a>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>





</body>

</html>